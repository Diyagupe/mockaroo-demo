package com.mockaroo.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mockaroo.demo.feign.ClienteFeign;
import com.mockaroo.demo.modelo.Clientes;

@SpringBootTest
class MockarooDemoApplicationTests {

	@Autowired
	private ClienteFeign clienteFeign;
	
	@Test
	void contextLoads() {
		List<Clientes> listica = clienteFeign.getAll();
		assertNotNull(listica);
	}

}
